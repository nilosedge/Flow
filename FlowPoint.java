import java.awt.*;



public class FlowPoint extends FlowObject {

	public FlowPoint(int width, int height) {
		super(width, height);
	}

	public void move() {

		if(x > width || x < 0) { xspeed = -xspeed; }
		if(y > height || y < 0) { yspeed = -yspeed; }
		x += xspeed;
		y += yspeed;

	}

	public void drawself(Graphics g) {
		g.setColor(c);
		g.drawRect((int)x, (int)y, 3, 3);
	}

}
