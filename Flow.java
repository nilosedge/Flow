import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;



public class Flow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 948223278510675838L;
	int width = 600;
	int height = 600;

	public Flow() {
		setTitle("Flow App");
		setLocation(0, 0);
		setBackground(Color.black);
		setSize(width, height);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(new FlowHandle(width, height), BorderLayout.CENTER);
		setVisible(true);

		while(true) {
			repaint();
//			try {
//				Thread.sleep(10);
//			} catch(Exception e) {
//
//			}
		}
		
	}

	public static void main(String[] args) {
		Flow fl;
		fl = new Flow();
	}

}
