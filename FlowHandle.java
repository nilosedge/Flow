import java.awt.*;
import javax.swing.*;

public class FlowHandle extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8626263167658401943L;
	FlowPoint flowpoints[];
	int width, height;
	int numberOfPoints = 2;

	public FlowHandle(int width, int height) {
		this.width = width;
		this.height = height;
		setBackground(Color.black);

		flowpoints = new FlowPoint[numberOfPoints];
		for(int i = 0; i < numberOfPoints; i++) {
			flowpoints[i] = new FlowPoint(width, height);
			flowpoints[i].setColor(Color.blue);
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for(int i = 0; i < numberOfPoints; i++) {
			flowpoints[i].move();
			flowpoints[i].drawself(g);
		}
	}

}
