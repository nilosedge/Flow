/**
* Class for hold the information of a 3 dimential point.
*/
public class WorldPoint {

	/**
	* Double value that holds the x portion of the point.
	*/
	public double x;
	/**
	* Double value that holds the y portion of the point.
	*/
	public double y;
	/**
	* Double value that holds the z portion of the point.
	*/
	public double z;

	/**
	* Construtor used for creating a point based on a incoming world point.
	* @param pt is a reference to the incoming world point 
	*/
	public WorldPoint(WorldPoint pt) {
		x = pt.x;
		y = pt.y;
		z = pt.z;
	}
	
	/**
	* Construtor used for creating a point based on 3 incoming values.
	* @param _x used to set the x value of this point.
	* @param _y used to set the y value of this point.
	* @param _z used to set the z value of this point.
	*/
	public WorldPoint(double _x, double _y, double _z) {
		x = _x;
		y = _y;
		z = _z;
	}

	/**
	* Funciton used to set a new location for this point.
	* @param newx is the new x value for x.
	* @param newy is the new y value for y.
	* @param newz is the new z value for z.
	*/
	public void set_pos(double newx, double newy, double newz) {
		x = newx;
		y = newy;
		z = newz;
	}
	
	/**
	* Function used to copy this point to the incoming point.
	* @param pt is  a reference to the point that this point will be copied to.
	*/
	public void copy(WorldPoint pt) {
		pt.x = x;
		pt.y = y;
		pt.z = z;
	}

	public void print_self() {
		System.out.println("(" + x + "," + y + "," + z + ")");
	}
}

